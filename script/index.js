// сделать калькулятор, который принимает два числа и знак арифметической операции и колбеком возвращает результат, при делении на 0 и вводе не символов - ошибка.

function calculate(number1, number2, callback) {  // функция calculate со значениями 
   return callback(number1, number2);
}

// функции арифметических операций с возвратом результата операции:

function sum(number1, number2) {
   return number1 + number2;
}
function subtraction(number1, number2) {
   return number1 - number2;
}
function multiple(number1, number2) {
   return number1 * number2;
}
function division(number1, number2) {
   if (number1 === 0 || number2 === 0) {      // Условие при делении на 0 появится алерт с ошибкой.
      alert('На 0 делить нельзя!');
   }
   return number1 / number2;
}


let number1 = parseInt(prompt('Введите первое число.'));   //функции ввода данных от пользователя в виде числе и арфиметической операции.
let number2 = parseInt(prompt('Введите второе число'));
let operation = prompt('Введите арифметическую операцию: " + ", " - ", " * ", " / "');

switch (operation) {               //Условие с перебором методом switch для выбора варианта операции, выходя из получанных данных.
   case '+':
      document.write('Результат додавання: ' + calculate(number1, number2, sum));   //выводы результата на экран
      break;
   case '-':
      document.write('Результат вiднiмання: ' + calculate(number1, number2, subtraction));
      break;
   case '*':
      document.write('Результат множення: ' + calculate(number1, number2, multiple));
      break;
   case '/':
      document.write('Результат дiлення: ' + calculate(number1, number2, division));
      break;
   case String:
      document.write('Error');
      break;
   default:
      document.write('Ошибка!');
      break;
}


document.write("<hr color='red'>")                                                  //Вывод красной линии на экран

//Сделать функцию map которая принимает 2 аргумента - колбек и элемент массива для преобразования этого масива.

let arr = [1, 2, 3, 4, 5];                                                          //Произвольный массив

function map(array, callback) {                                                     //функция map с колбеком функции и вложенным елементом массива.
   let arr2 = [];
   for (let i = 0; i < array.length; i++) {
      arr2.push(callback(array[i]));
   }
   return arr2;                                                                     //Возврат значение преобразования элементов
}

function multiple(x) {                                                              //функция возврата значение.
   return x * x;
}
console.log(map(arr, multiple));                                                    //вывод в консоль полученного результата
document.write('Результат роботи "map": ' + map(arr, multiple));                    //вывод на экран полученного результата.